/**
 * @file	main.cpp
 * @brief	Função principal do programa
 * @author	Rodolpho Erick 	(rodolphoerick90@gmail.com)
 * @since	07/06/2017
 * @date	08/06/2017
 * @sa 		cp.h
 * @sa 		cc.h
 * @sa 		conta.h
 */

#include "cp.h"
#include "cc.h"
#include "conta.h"
#include "menu.h"

#include <iostream>
using std::cin;
using std::cout;
using std::endl;

#include <string>
using std::string;

/** @brief 	Função principal */
int main() {
	Cc *corrente = new Cc();
	Cp *poupanca = new Cp();

	escolhaInicial(*corrente, *poupanca);

	return 0;
}