/**
 * @file	cc.cpp
 * @brief	Implementacao dos metodos da classe Cc para representar a
 *			conta corrente de um cliente
 * @author	Rodolpho Erick 	(rodolphoerick90@gmail.com)
 * @since	07/06/2017
 * @date	08/06/2017
 * @sa 		cc.h
 */

#include "cc.h"
#include "conta.h"

/** @brief 	Construtor padrão */
Cc::Cc() {
	
}

/**
 * @param de  Valor que será depositado em conta corrente
 * @param sa  Valor que será sacado em conta corrente
 * @param lim Limite de saque em caso de saldo negativo
 */
Cc::Cc(double de, double saq, double lim) {
	setDeposito(de);
	setSaque(saq);
	setLimite(lim);
	setDia();
}

/** @brief 	Destrutor padrao */
Cc::~Cc() {

}

/** @brief Retorna o saldo da conta corrente */
double Cc::getSaldo() {
	return saldo;
}

/** @brief Modifica o saldo da conta corrente. PERIGO! ÁREA RESTRITA! */
void Cc::setSaldo(double sa) {
	saldo += sa;	
}

/** @brief 	Retorna o depósito em conta corrente */
double Cc::getDeposito() {
	return deposito;
}

/** @brief 	Modifica o depósito feito em conta corrente */
void Cc::setDeposito(double de) {
	deposito = de;
	setSaldo(de);
}

/** @brief 	Retorna o depósito em conta corrente */
double Cc::getSaque() {
	return saque;
}

/** @brief 	Modifica o depósito feito em conta corrente */
void Cc::setSaque(double saq) {
	double saldoF = saldo - saq;

	if (saldoF < limite) {
		cerr << "Desculpe, saque negado! O seu saldo é insuficiente para realizar a operação.";
	} else {
		saldo = saldoF;		
	}

	saque = saq;
}


/** @return Juros aplicados sobre o saldo */
double Cc::getJurosP() {
	return jurosP;
}

/** @brief 	Modifica os juros aplicados sobre o saldo */
void Cc::setJurosP() {
	if (saldo > 0) {
		jurosP += 0.05;
	} 
}

/** @return Juros aplicados sobre o saldo negativo */
double Cc::getJurosN() {
	return jurosN;
}

/** @brief 	Modifica os juros aplicados sobre o saldo negativo */
void Cc::setJurosN() {
	if (saldo < 0) {
		jurosN += 0.05;
	} 
}

/** @brief Retorna o dia de um mes */
short Cc::getDia() {
	return dia;
}
/** @brief Modifica o dia de um mes */
void Cc::setDia() {
	if (dia == 30) {
		dia = 1;
		setMes();
		setJurosP();
		setJurosN();
	} else {
		dia += 1;
		setJurosP();
		setJurosN();
	}
}

/** @brief Retorna o mes de um ano */
short Cc::getMes() {
	return mes;
}

/** @brief Modifica o mes de um ano */
void Cc::setMes() {
	if (mes == 12) {
		mes = 1;
		setAno();
	} else {
		mes +=1;
	}
}

/** @brief 	Retorna o ano */
int Cc::getAno() {
	return ano;
}

/** @brief 	Modifica o ano */
void Cc::setAno() {
	ano += 1;
}

/** @brief 	Retorna limite de saque em caso de saldo negativo */
double Cc::getLimite() {
	return limite;
}

/** @brief 	Modifica limite de saque em caso de saldo negativo */
void Cc::setLimite(double lim) {
	limite = lim;
}

/** @brief 	Atualiza o saldo da conta ao aplicar os juros */
double Cc::atualiza() {
	setDia();
	if (saldo > 0) {
		saldo += saldo * jurosP;
	} else {
		saldo -= saldo * jurosN;
	}

	return saldo;
}
