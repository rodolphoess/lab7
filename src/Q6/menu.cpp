/**
* @file menu.cpp
* @brief	Arquivo de corpo com as implementacoes das funcoes para os menus
* @author	Rodolpho Erick 	(rodolphoerick90@gmail.com)
* @since 	08/05/2017
* @date 	08/05/2017
* @sa 		menu.h
*/

#include "menu.h"

/** @brief Função para escolha inicial das operações */ 
void escolhaInicial(Cc &corrente, Cp &poupanca) {
	int opcao = menuPrincipal();

	int opcao1, opcao2;
	switch(opcao) {
		case 0:

			cout << "Programa encerrando!" << endl;
			break;

		case 1:
			opcao1 = menuCorrente();
			operacaoCorrente(opcao1, corrente, poupanca);
			break;

		case 2:
			opcao2 = menuPoupanca();
			operacaoPoupanca(opcao2, poupanca, corrente);
			break;
	}
}

/**
* @brief 	Menu para escolha da operação
*/
int menuPrincipal() {
	cout << "____________________________________________________________________ "  << endl;
	cout << "\t			        Bem Vindo 			" << endl;
	cout << "\t									" << endl;
	cout << "\t		Escolha uma das opções listadas abaixo:		" << endl;
	cout << "\t									" << endl;
	cout << "\t		(1) Conta Corrente 				" << endl;
	cout << "\t									" << endl;
	cout << "\t		(2) Conta Poupança				" << endl;
	cout << "\t									" << endl;	
	cout << "\t		(0) Sair 						" << endl;
	cout << "____________________________________________________________________ "  << endl;
	cout << endl;

	int opcao = 0;

	do {
		cin >> opcao;
		return opcao;
	} while(opcao != 0 || opcao < 3);
}

/**
* @brief 	menu para conta corrente
*/
int menuCorrente() {
	cout << "____________________________________________________________________ "  << endl;
	cout << "\t			Conta Corrente					" << endl;
	cout << "\t									" << endl;
	cout << "\t		Escolha uma das opções listadas abaixo:		" << endl;
	cout << "\t									" << endl;
	cout << "\t		(1) Definir limite para saque				" << endl;
	cout << "\t									" << endl;
	cout << "\t		(2) Realizar depósito			" << endl;
	cout << "\t									" << endl;
	cout << "\t		(3) Realizar saque		" << endl;
	cout << "\t									" << endl;
	cout << "\t		(4) Visualizar saldo		 			" << endl;
	cout << "\t									" << endl;
	cout << "\t		(5) Aplicar juros		 			" << endl;
	cout << "\t									" << endl;
	cout << "\t		(0) Voltar 						" << endl;
	cout << "____________________________________________________________________ "  << endl;
	cout << endl;

	int opcao = 0;

	do {
		cin >> opcao;
		return opcao;
	} while(opcao != 0 || opcao < 6);
}

/**
* @brief    menu para conta poupança
*/
int menuPoupanca() {
	cout << "____________________________________________________________________ "  << endl;
	cout << "\t				Conta Poupança				" << endl;
	cout << "\t									" << endl;
	cout << "\t		Escolha uma das opções listadas abaixo:			" << endl;	
	cout << "\t									" << endl;
	cout << "\t		(1) Realizar depósito			" << endl;
	cout << "\t									" << endl;
	cout << "\t		(2) Realizar saque		" << endl;
	cout << "\t									" << endl;
	cout << "\t		(3) Visualizar saldo		 			" << endl;
	cout << "\t									" << endl;
	cout << "\t		(4) Aplicar juros		 			" << endl;
	cout << "\t									" << endl;
	cout << "\t		(0) Voltar 						" << endl;
	cout << "____________________________________________________________________ "  << endl;
	cout << endl;

	int opcao = 0;

	do {
		cin >> opcao;
		return opcao;
	} while(opcao != 0 || opcao < 5);
}

/** @brief Operações sobre a conta corrente */ 
void operacaoCorrente(int opcao, Cc &corrente, Cp &poupanca){	

	if (opcao == 1) {
		double limite;
		cout << "Define um limite negativo para saque na conta: ";
		cin >> limite;

		corrente.setLimite(limite);

		opcao = menuCorrente();
		operacaoCorrente(opcao, corrente, poupanca);
	} else if (opcao == 2) {
		double deposito;
		cout << "Defina o valor do depósito: ";
		cin >> deposito;

		corrente.setDeposito(deposito);

		opcao = menuCorrente();
		operacaoCorrente(opcao, corrente, poupanca);
	} else if (opcao == 3) {
		double saque;
		cout << "Defina o valor do saque: ";
		cin >> saque;

		corrente.setSaque(saque);

		opcao = menuCorrente();
		operacaoCorrente(opcao, corrente, poupanca);
	} else if (opcao == 4) {
		cout << "Seu saldo é de R$" << corrente.getSaldo() << endl;

		opcao = menuCorrente();
		operacaoCorrente(opcao, corrente, poupanca);
	} else if (opcao == 5) {
		corrente.atualiza();
		cout << "Juros aplicados sobre a conta. Saldo atual: R$" << corrente.getSaldo() << endl;

		opcao = menuCorrente();
		operacaoCorrente(opcao, corrente, poupanca);
 	} else if (opcao == 0) {
 		escolhaInicial(corrente, poupanca);
 	}
}

/** @brief Operações sobre a conta poupança */ 
void operacaoPoupanca(int opcao, Cp &poupanca, Cc &corrente){	

	if (opcao == 1) {
		double deposito;
		cout << "Defina o valor do depósito: ";
		cin >> deposito;

		poupanca.setDeposito(deposito);

		opcao = menuPoupanca();
		operacaoPoupanca(opcao, poupanca, corrente);
	} else if (opcao == 2) {
		double saque;
		cout << "Defina o valor do saque: ";
		cin >> saque;

		poupanca.setSaque(saque);

		opcao = menuPoupanca();
		operacaoPoupanca(opcao, poupanca, corrente);
	} else if (opcao == 3) {
		cout << "Seu saldo é de R$" << poupanca.getSaldo() << endl;

		opcao = menuPoupanca();
		operacaoPoupanca(opcao, poupanca, corrente);
	} else if (opcao == 4) {
		poupanca.atualiza();
		cout << "Juros aplicados sobre a conta. Saldo atual: R$" << poupanca.getSaldo() << endl;
		
		opcao = menuPoupanca();
		operacaoPoupanca(opcao, poupanca, corrente);
 	} else if (opcao == 0) {
 		escolhaInicial(corrente, poupanca);
 	}
}

