/**
 * @file	cp.cpp
 * @brief	Implementacao dos metodos da classe Cp para representar a
 *			conta poupança de um cliente
 * @author	Rodolpho Erick 	(rodolphoerick90@gmail.com)
 * @since	07/06/2017
 * @date	08/06/2017
 * @sa 		cp.h
 */

#include "cp.h"
#include "conta.h"

/** @brief 	Construtor padrão */
Cp::Cp() {
	
}

/**
 * @param de  Valor que será depositado em conta corrente
 * @param sa  Valor que será sacado em conta corrente
 */
Cp::Cp(double de, double saq) {
	setDeposito(de);
	setSaque(saq);	
	setDia();
}

/** @brief 	Destrutor padrao */
Cp::~Cp() {

}

/** @brief Retorna o saldo da conta corrente */
double Cp::getSaldo() {
	return saldo;
}

/** @brief Modifica o saldo da conta corrente. PERIGO! ÁREA RESTRITA! */
void Cp::setSaldo(double sa) {
	saldo += sa;	
}

/** @brief 	Retorna o depósito em conta corrente */
double Cp::getDeposito() {
	return deposito;
}

/** @brief 	Modifica o depósito feito em conta corrente */
void Cp::setDeposito(double de) {
	deposito = de;
	setSaldo(de);
}

/** @brief 	Retorna o depósito em conta corrente */
double Cp::getSaque() {
	return saque;
}

/** @brief 	Modifica o depósito feito em conta corrente */
void Cp::setSaque(double saq) {
	double saldoF = saldo - saq;

	if (saldoF <= 0) {
		cerr << "Desculpe, saque negado! O seu saldo é insuficiente para realizar a operação.";
	} else {
		saldo = saldoF;		
	}

	saque = saq;
}


/** @return Juros aplicados sobre o saldo */
double Cp::getJurosP() {
	return jurosP;
}

/** @brief 	Modifica os juros aplicados sobre o saldo */
void Cp::setJurosP() {
	if (saldo > 0) {
		jurosP += 0.025;
	} 
}

/** @brief Retorna o dia de um mes */
short Cp::getDia() {
	return dia;
}
/** @brief Modifica o dia de um mes */
void Cp::setDia() {
	if (dia == 30) {
		dia = 1;
		setMes();
		setJurosP();		
	} else {
		dia += 1;
		setJurosP();		
	}
}

/** @brief Retorna o mes de um ano */
short Cp::getMes() {
	return mes;
}

/** @brief Modifica o mes de um ano */
void Cp::setMes() {
	if (mes == 12) {
		mes = 1;
		setAno();
	} else {
		mes +=1;
	}
}

/** @brief 	Retorna o ano */
int Cp::getAno() {
	return ano;
}

/** @brief 	Modifica o ano */
void Cp::setAno() {
	ano += 1;
}

/** @brief 	Atualiza o saldo da conta ao aplicar os juros */
double Cp::atualiza() {	
	saldo += saldo * jurosP;	

	return saldo;
}
