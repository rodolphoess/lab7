/**
 * @file	main5.cpp
 * @brief	Função principal do programa 5
 * @author	Silvio Sampaio 
 * @author	Everton Ranielly
 * @since	08/06/2017
 * @date	08/06/2017 
 */

#include <iostream>
using std::cout;
using std::endl;

#include <iterator>
using std::back_inserter;

#include <vector>
using std::vector;

#include <algorithm>
using std::transform;

int square(int val) {
	return val * val;
}

int main(int argc, char* argv[]) {
	vector<int> col; /**< Declara um container do tipo vector que receberá inteiros */
	vector<int> col2; /**< Declara um outro container do tipo vector que também receberá inteiros */

	for (int i = 1; i <= 9; ++i) {	/**< Loop para adicionar elementos no vector col */
		col.push_back(i);
	}

	/** Adiciona do final para o início do vector col2, os elementos do vector col
 	 *	elevados ao quadrado	
	 */
	transform(col.begin(), col.end(), back_inserter(col2), square);

	/**< Imprimre os elementos do vector col2 desde a sua primeira posição até a última */
	for (vector<int>::iterator it = col2.begin(); it != col2.end(); ++it) {
		cout << (*it) << " ";
	}

	cout << endl;

	return 0;
}