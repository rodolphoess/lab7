/**
 * @file	menu.h
 * @brief	Definicao das funções para o menu de opções do usuário
 * @author	Rodolpho Erick 	(rodolphoerick90@gmail.com)
 * @since	04/06/2017
 * @date	08/06/2017
 */

#ifndef MENU_H
#define MENU_H

#include "cc.h"
#include "cp.h"

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

/** @brief Função para escolha inicial das operações */ 
void escolhaInicial(Cc &corrente, Cp &poupanca);

/** @brief Menu principal */ 
int menuPrincipal();

/** @brief Menu da conta corrente */ 
int menuCorrente();

/** @brief Menu da conta poupança */ 
int menuPoupanca();

/** @brief Operações sobre a conta corrente */ 
void operacaoCorrente(int opcao, Cc &corrente, Cp &poupanca);

/** @brief Operações sobre a conta poupança */ 
void operacaoPoupanca(int opcao, Cp &poupanca, Cc &corrente);

#endif