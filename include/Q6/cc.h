/**
 * @file	cc.h
 * @brief	Definicao da classe Cc para representar a conta corrente de um cliente
 * @details Essa classe possui como atributos o saldo, juros, data, juros e limite
 * @author	Rodolpho Erick 	(rodolphoerick90@gmail.com)
 * @since	04/06/2017
 * @date	08/06/2017
 */

#ifndef CC_H
#define CC_H

#include "conta.h"

#include <iostream>
using std::cout;
using std::cerr;

/**
 * @class 	Classe Cc
 * @brief 	Definicao da classe Cc para representar a conta corrente de um cliente
 * @details Essa classe possui como atributos o saldo, juros, data
 */
class Cc : public Conta {
	private:
		double saldo;
		double deposito;
		double saque;
		double jurosP = 0.01;
		double jurosN = 0.05;
		short dia = 1;
		short mes = 1;
		int ano = 2017;
		double limite;

		/** @brief 	Modifica o saldo da conta corrente. Nao pode ser publico
		 *			pois o saldo nao pode ser modificado livremente. Por isso, é um
		 *			método descartável, mas deixarei por fins pedagógicos.
		 */
		void setSaldo(double sa);	

	public:
		/** @brief 	Construtor padrao */
		Cc();

		/** @brief Construtor parametrizado */
		Cc(double de, double saq, double lim);

		/** @brief 	Destrutor padrao */
		~Cc();

		/** @brief 	Retorna o saldo da conta corrente */
		double getSaldo();		

		/** @brief 	Retorna o depósito em conta corrente */
		double getDeposito();

		/** @brief 	Modifica o depósito feito em conta corrente */
		void setDeposito(double de);

		/** @brief 	Retorna o depósito em conta corrente */
		double getSaque();

		/** @brief 	Modifica o depósito feito em conta corrente */
		void setSaque(double saq);

		/** @brief 	Retorna os juros aplicados sobre o saldo positivo */
		double getJurosP();

		/** @brief 	Modifica os juros aplicados sobre o saldo positivo */
		void setJurosP(); 

		/** @brief 	Retorna os juros aplicados sobre o saldo negativo */
		double getJurosN();

		/** @brief 	Modifica os juros aplicados sobre o saldo negativo */
		void setJurosN();

		/** @brief Retorna o dia de um mes */
		short getDia();

		/** @brief Modifica o dia de um mes */
		void setDia();

		/** @brief Retorna o mes de um ano */
		short getMes();

		/** @brief Modifica o mes de um ano */
		void setMes();

		/** @brief 	Retorna o ano */
		int getAno();

		/** @brief 	Modifica o ano */
		void setAno();

		/** @brief 	Retorna limite de saque em caso de saldo negativo */
		double getLimite();

		/** @brief 	Modifica o limite de saque em caso de saldo negativo */
		void setLimite(double lim);

		/** @brief 	Atualiza o saldo da conta ao aplicar os juros */
		double atualiza();
};

#endif