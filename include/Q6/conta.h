/**
 * @file	conta.h
 * @brief	Definicao da classe Conta como interface para representar 
 *			métodos básicos da conta de um cliente
 * @author	Rodolpho Erick 	(rodolphoerick90@gmail.com)
 * @since	07/06/2017
 * @date	08/06/2017
 */

#ifndef CONTA_H
#define CONTA_H

/**
 * @class 	Classe Conta
 * @brief 	Definicao da classe Conta como interface para representar 
 *			métodos básicos da conta de um cliente
 */

class Conta {
	public:
		/** @brief 	Retorna o saldo da conta corrente */
		virtual double getSaldo() = 0;		

		/** @brief 	Retorna o depósito em conta corrente */
		virtual double getDeposito() = 0;

		/** @brief 	Modifica o depósito feito em conta corrente */
		virtual void setDeposito(double de) = 0;

		/** @brief 	Retorna o depósito em conta corrente */
		virtual double getSaque() = 0;

		/** @brief 	Modifica o depósito feito em conta corrente */
		virtual void setSaque(double saq) = 0;

		/** @brief 	Retorna os juros aplicados sobre o saldo positivo */
		virtual double getJurosP() = 0;

		/** @brief 	Modifica os juros aplicados sobre o saldo positivo */
		virtual void setJurosP() = 0;		

		/** @brief Retorna o dia de um mes */
		virtual short getDia() = 0;

		/** @brief Modifica o dia de um mes */
		virtual void setDia() = 0; 

		/** @brief Retorna o mes de um ano */
		virtual short getMes() = 0;

		/** @brief Modifica o mes de um ano */
		virtual void setMes() = 0; 

		/** @brief 	Retorna o ano */
		virtual int getAno() = 0;

		/** @brief 	Modifica o ano */
		virtual void setAno() = 0; 

		/** @brief 	Atualiza o saldo da conta ao aplicar os juros */
		virtual double atualiza() = 0;
};

#endif