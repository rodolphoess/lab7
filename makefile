# Exemplo completo de um Makefile, separando os diferentes elementos da aplicacao
# como codigo (src), cabecalhos (include), executaveis (build), bibliotecas (lib), etc.
# Cada elemento e alocado em uma pasta especifica, organizando melhor seu codigo fonte.
#
# Algumas variaveis sao usadas com significado especial:
#
# $@ nome do alvo (target)
# $^ lista com os nomes de todos os pre-requisitos sem duplicatas
# $< nome do primeiro pre-requisito
#

# Comandos do sistema operacional
# Linux: rm -rf 
# Windows: cmd //C del 
RM = rm -rf 

# Compilador
CC=g++ 

# Variaveis para os subdiretorios
LIB_DIR = ./lib
INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./build
DOC_DIR = ./doc
TEST_DIR = ./test

# Nome do programa
PROG1 =
PROG2 =
PROG3 =
PROG4 =
PROG5 =
PROG6 = banco

# Opcoes de compilacao
CFLAGS=-Wall -pedantic -ansi -std=c++11 -I.

# Garante que os alvos desta lista nao sejam confundidos com arquivos de mesmo nome
.PHONY: init all clean doxy debug doc

# Define o alvo (target) para a compilacao completa.
# Ao final da compilacao, remove os arquivos objeto.
all: $(PROG1) $(PROG2) $(PROG3) $(PROG4) $(PROG5) $(PROG6)

debug: CFLAGS += -g -O0
debug: $(PROG1) $(PROG2) $(PROG3) $(PROG4) $(PROG5) $(PROG6)


# Alvo (target) para a criação da estrutura de diretorios
# necessaria para a geracao dos arquivos objeto 
init:	
	@mkdir -p $(OBJ_DIR)/Q1
	@mkdir -p $(OBJ_DIR)/Q2
	@mkdir -p $(OBJ_DIR)/Q3
	@mkdir -p $(OBJ_DIR)/Q4
	@mkdir -p $(OBJ_DIR)/Q5
	@mkdir -p $(OBJ_DIR)/Q6
	@mkdir -p $(BIN_DIR)/Q1
	@mkdir -p $(BIN_DIR)/Q2
	@mkdir -p $(BIN_DIR)/Q3
	@mkdir -p $(BIN_DIR)/Q4
	@mkdir -p $(BIN_DIR)/Q5
	@mkdir -p $(BIN_DIR)/Q6


# Alvo (target) para a construcao do executavel banco
# Define os arquivos *.o como dependencias
$(PROG6): CFLAGS += -I$(INC_DIR)/Q6
$(PROG6): BIN_DIR=./bin/Q6
$(PROG6): $(OBJ_DIR)/Q6/cc.o $(OBJ_DIR)/Q6/cp.o $(OBJ_DIR)/Q6/menu.o $(OBJ_DIR)/Q6/main.o
	@echo "============="
	@echo "Ligando o alvo $@"
	@echo "============="
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^
	@echo "+++ [Executavel 'banco' criado em $(BIN_DIR)] +++"
	@echo "============="

# Alvo (target) para a construcao do objeto cc.o
# Define os arquivos cc.cpp e cc.h como dependencias.
$(OBJ_DIR)/Q6/cc.o: $(SRC_DIR)/Q6/cc.cpp $(INC_DIR)/Q6/cc.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a construcao do objeto cp.o
# Define os arquivos cp.cpp e cp.h como dependencias.
$(OBJ_DIR)/Q6/cp.o: $(SRC_DIR)/Q6/cp.cpp $(INC_DIR)/Q6/cp.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a construcao do objeto menu.o
# Define os arquivos menu.cpp e menu.h como dependencias.
$(OBJ_DIR)/Q6/menu.o: $(SRC_DIR)/Q6/menu.cpp $(INC_DIR)/Q6/menu.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a construcao do objeto mainRecursivo.o
# Define os arquivos mainRecursivo.cpp como dependencias.
$(OBJ_DIR)/Q6/main.o: $(SRC_DIR)/Q6/main.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo (target) para a geração automatica de documentacao usando o Doxygen.
# Sempre remove a documentacao anterior (caso exista) e gera uma nova.
doxy:
	$(RM) $(DOC_DIR)/*
	doxygen Doxyfile

# Alvo (target) usado para limpar os arquivos temporarios (objeto)
# gerados durante a compilacao, assim como os arquivos binarios/executaveis.
clean:
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*